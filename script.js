function progress() {
    var html =  document.getElementById("html").innerText
    $("#html").css("width", html)
    $("#html").css("background-color", "red")
    var css =  document.getElementById("css").innerText
    $("#css").css("width", css)
    $("#css").css("background-color", "blue")
    var js =  document.getElementById("js").innerText
    $("#js").css("width", js)
    $("#js").css("background-color", "yellow")
    var php =  document.getElementById("php").innerText
    $("#php").css("width", php)
    $("#php").css("background-color", "#8184B7")
    var bdd =  document.getElementById("bdd").innerText
    $("#bdd").css("width", bdd)
    $("#bdd").css("background-color", "#DF931F")
    var py =  document.getElementById("py").innerText
    $("#py").css("width", py)
    $("#py").css("background-color", "#356C99")
    var c =  document.getElementById("c").innerText
    $("#c").css("width", c)
    $("#c").css("background-color", "#318C81")
    var cpp =  document.getElementById("cpp").innerText
    $("#cpp").css("width", cpp)
    $("#cpp").css("background-color", "#6295CC")
    var java =  document.getElementById("java").innerText
    $("#java").css("width", java)
    $("#java").css("background-color", "#EF6401")
}
window.onload = progress;
